﻿using AutoMapper;
using Core;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using VenekBankDAL.Abstractions;
using VenekBankDAL.Entities;
using VenekeBankBLL.Abstractions;
using VenekeBankBLL.DomainModels.Users;

namespace VenekeBankBLL.Services
{
    public class UserAuthService : IUserAuthService
    {
        private readonly IUserAuthRepository _userAuthRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public UserAuthService(IUserAuthRepository userAuthRepository = null, IMapper mapper = null, IConfiguration configuration = null)
        {
            _userAuthRepository = userAuthRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<ServiceResponse<int>> Register(UserModel user, string password)
        {
            var response = new ServiceResponse<int>();

            try
            {
                var newUserData = _mapper.Map<User>(user);
                newUserData.CreatedBy = user.Username;
                newUserData.UpdatedBy = user.Username;

                response = await _userAuthRepository.Register(newUserData, password);
            }
            catch (Exception ex) 
            {
                response.IsSuccessfull = false;
                response.Data = 0;
                response.Message = $"Sorry We failed to create the new user. Error {ex.Message}";
            }

            return response;
        }

        public async Task<bool> UserExists(string username)
        {
            return await _userAuthRepository.UserExists(username);
        }

        public async Task<ServiceResponse<string>> Login (string username, string password)
        {
            var response = new ServiceResponse<string>();

            try
            {
                var user = await _userAuthRepository.GetUserByUsernameAsync(username);

                if (user == null)
                {
                    response.Message = "User is not recognized, please kindly provide the correct username and password";
                    return response;
                }

                if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                {
                    response.Message = "Wrong password.";
                }
                else
                {
                    response.IsSuccessfull = true;
                    response.Data = CreateToken(user);
                }
            }
            catch (Exception ex) 
            {
                response.IsSuccessfull = false;
                response.Message = $"Sorry we couldn't create this user {ex.Message}";
            }

            return response;
        }

        #region HELPER FUNCTIONS

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var computeHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                return computeHash.SequenceEqual(passwordHash);
            }
        }

        private string CreateToken(User user)
        {
            List<Claim> claims = new List<Claim> {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Username)
            };

            SymmetricSecurityKey key = new SymmetricSecurityKey(System.Text.Encoding.UTF8
                .GetBytes(_configuration.GetSection("AppSettings:Token").Value));

            SigningCredentials creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }

        #endregion
    }
}
