﻿using AutoMapper;
using Core;
using System.Collections.Generic;
using VenekBankDAL.Abstractions;
using VenekBankDAL.Entities;
using VenekeBankBLL.Abstractions;
using VenekeBankBLL.DomainModels.CommunityProjects;
using VenekeBankBLL.DomainModels.SponsorShip;
using VenekeBankDomain.Abstractions;

namespace VenekeBankDomain.Services;

public class CommunityProjectService : ICommunityProjectService
{
    private readonly IVenekeBankRepository _venekeBankRepository;
    private readonly IUserAuthService _userAuthService;
    private readonly IMapper _mapper;

    public CommunityProjectService(IVenekeBankRepository venekeBankRepository, IMapper mapper, IUserAuthService userAuthService)
    {
        _venekeBankRepository = venekeBankRepository;
        _mapper = mapper;
        _userAuthService = userAuthService;
    }

    public async Task<ServiceResponse<CommunityProjectsDto>> CreateCommunityProject(CommunityProjectsCreateDto createNewProject)
    {
        var serviceResponse = new ServiceResponse<CommunityProjectsDto>();

        try
        {
            if (!await _userAuthService.UserExists(createNewProject.CreatedBy))
            {
                serviceResponse.IsSuccessfull = false;
                serviceResponse.Data = default;
                serviceResponse.Message = "Sorry this user is not recognized";

                return serviceResponse;
            }

            var newProject = _mapper.Map<CommunityProject>(createNewProject);
            newProject.CreatedBy = createNewProject.CreatedBy;
            newProject.UpdatedBy = createNewProject.CreatedBy;

            var createdCommunityProject = await _venekeBankRepository.CreateCommunityProjectAsync(newProject);

            serviceResponse.IsSuccessfull = true;
            serviceResponse.Data = _mapper.Map<CommunityProjectsDto>(createdCommunityProject);
            serviceResponse.Message = "Successfully created";
        }
        catch (Exception ex)
        {
            serviceResponse.IsSuccessfull = false;
            serviceResponse.Data = null;
            serviceResponse.Message = $"Failed to retrieve the data expected {ex.Message}";
        }

        return serviceResponse;
    }


    public async Task<ServiceResponse<List<CommunityProjectsDto>>> GetAllCommunityProjectsAsync()
    {
        var serviceResponse = new ServiceResponse<List<CommunityProjectsDto>>();

        try
        {
            var postalCodes = await _venekeBankRepository.GetAllCommunityProjectsAsync();

            serviceResponse.IsSuccessfull = true;
            serviceResponse.Data = _mapper.Map<List<CommunityProjectsDto>> (postalCodes);
            serviceResponse.Message = serviceResponse.Data.Count > 0 ? 
                            "Successfully retrieved" : "No data loaded ,please create the projects.";
        }
        catch (Exception ex)
        {
            serviceResponse.IsSuccessfull = false;
            serviceResponse.Data = null;
            serviceResponse.Message = $"Failed to retrieve the data expected {ex.Message}";
        }

        return serviceResponse;
    }

    public async Task<ServiceResponse<List<CommunityProjectsDto>>> GetCommunityProjectsByUserNameAsync(string username)
    {
        var serviceResponse = new ServiceResponse<List<CommunityProjectsDto>>();

        try
        {
            if (!await _userAuthService.UserExists(username))
            {
                serviceResponse.IsSuccessfull = false;
                serviceResponse.Data = default;
                serviceResponse.Message = "Sorry this user is not recognized";

                return serviceResponse;
            }

            var postalCodes = await _venekeBankRepository.GetClientCommunityProjectsAsync(username);

            serviceResponse.IsSuccessfull = true;
            serviceResponse.Data = _mapper.Map<List<CommunityProjectsDto>>(postalCodes);
            serviceResponse.Message = "Successfully retrieved";
        }
        catch (Exception ex)
        {
            serviceResponse.IsSuccessfull = false;
            serviceResponse.Data = null;
            serviceResponse.Message = $"Failed to retrieve the data expected {ex.Message}";
        }

        return serviceResponse;
    }
}
