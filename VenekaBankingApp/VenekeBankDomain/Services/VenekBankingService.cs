﻿using AutoMapper;
using Core;
using System.Runtime.InteropServices;
using VenekBankDAL.Abstractions;
using VenekBankDAL.Entities;
using VenekeBankBLL.Abstractions;
using VenekeBankBLL.DomainModels.SponsorShip;

namespace VenekeBankBLL.Services
{
    public class VenekBankingService : IVenekBankingService
    {
        private readonly IVenekeBankRepository _venekeBankRepository;
        private readonly IUserAuthRepository _userAuthRepository;
        private readonly IMapper _mapper;

        public VenekBankingService(IVenekeBankRepository venekeBankRepository, IUserAuthRepository userAuthRepository, IMapper mapper)
        {
            _venekeBankRepository = venekeBankRepository;
            _userAuthRepository = userAuthRepository;
            _mapper = mapper;
        }

        public async Task<ServiceResponse<SponsorshipPlanModel>> CreateSponserShipPlan(CreateSponsorshipPlanDto createSponsorship)
        {
            var response = new ServiceResponse<SponsorshipPlanModel>();
            response.Data = null;

            try
            {
                var communityProject = await _venekeBankRepository.GetCommunityProjectByIdAsync(createSponsorship.CommunityProjectId);

                if (communityProject is null)
                {
                    response.IsSuccessfull = false;
                    response.Message = "We cant find the project you listed";
                    return response;
                }

                var accountUser = await _userAuthRepository.GetUserByUsernameAsync(createSponsorship.UserName);

                if (accountUser is null)
                {
                    response.IsSuccessfull = false;
                    response.Message = "User does not exists";
                    return response;
                }

                var sponsorShipRequest = new SponsorshipPlan()
                {
                    User = accountUser,
                    CreatedBy = createSponsorship.UserName,
                    UpdatedBy = createSponsorship.UserName,
                    CommunityProject = communityProject,
                    SourceOfFunds = createSponsorship.SourceOfFunds,
                    PaymentFrequency = createSponsorship.PaymentFrequency,
                    StartDate = createSponsorship.StartDate,
                    FinishDate = createSponsorship.FinishDate,
                };

                var result = await _venekeBankRepository.CreateSponsorshipPlanAsync(sponsorShipRequest);

                if (result is not null)
                {
                    response.IsSuccessfull = true;
                    response.Message = "Sponsorship Plan created";
                    response.Data = _mapper.Map<SponsorshipPlanModel>(result);
                }
            }
            catch (Exception ex)
            {
                response.IsSuccessfull = false;
                response.Message = $"Failed to create the Sponsorship Plan {ex.Message} ";
                return response;
            }

            return response;

        }

        public async Task<ServiceResponse<List<SponsorshipPlanModel>>> GetClientSponsorShipPlans(string userName)
        {
            var response = new ServiceResponse<List<SponsorshipPlanModel>>();
            response.Data = null;

            try
            {
                List<SponsorshipPlan> result = await _venekeBankRepository.GetClientSponsorShipPlansAsync(userName);

                if (result is not null)
                {
                    response.IsSuccessfull = true;
                    response.Message = "Found the list of client sponsorship plans";
                    response.Data = _mapper.Map<List<SponsorshipPlanModel>>(result);
                }
                else 
                {
                    response.IsSuccessfull = false;
                    response.Message = $"Failed to get of client sponsorship plans";
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.IsSuccessfull = false;
                response.Message = $"Failed to find list of client sponsorship plans Error: {ex.Message} ";
                return response;
            }

            return response;
        }
    }
}
