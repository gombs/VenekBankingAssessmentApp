﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VenekBankDAL;
using VenekeBankBLL.Abstractions;
using VenekeBankBLL.AutoMapperProfile;
using VenekeBankBLL.Services;
using VenekeBankDomain.Abstractions;
using VenekeBankDomain.Services;

namespace VenekeBankBLL;

public static class Extensions
{
    public static IServiceCollection AddVenekeBankBLLServiceCollections(this IServiceCollection servicesCollection, IConfiguration configuration)
    {
        var mapperConfig = new MapperConfiguration(mc =>
        {
            mc.AddProfile(new VenekaBankMappingProfile());
        });
        var mapper = mapperConfig.CreateMapper();

        return servicesCollection
           .AddSingleton(mapper)
           .AddVenekeBankDALServiceCollections(configuration)
           .AddTransient<ICommunityProjectService, CommunityProjectService>()
           .AddTransient<IVenekBankingService, VenekBankingService>()
           .AddTransient<IUserAuthService, UserAuthService>();
    }
}
