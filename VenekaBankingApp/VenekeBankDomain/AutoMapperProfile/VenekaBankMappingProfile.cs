﻿using AutoMapper;
using VenekBankDAL.Entities;
using VenekeBankBLL.DomainModels.CommunityProjects;
using VenekeBankBLL.DomainModels.SponsorShip;
using VenekeBankBLL.DomainModels.Users;

namespace VenekeBankBLL.AutoMapperProfile;

public class VenekaBankMappingProfile : Profile
{
    public VenekaBankMappingProfile()
    {
        CreateMap<CommunityProject, CommunityProjectsDto>().ReverseMap();

        CreateMap<CommunityProject, CommunityProjectsCreateDto>().ReverseMap();

        CreateMap<UserModel, User>().ReverseMap();

        CreateMap<SponsorshipPlan, SponsorshipPlanModel>().ReverseMap();
    }
}
