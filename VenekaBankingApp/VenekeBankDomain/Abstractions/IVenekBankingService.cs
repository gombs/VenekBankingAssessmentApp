﻿using Core;
using VenekeBankBLL.DomainModels.SponsorShip;

namespace VenekeBankBLL.Abstractions;

public interface IVenekBankingService
{
    Task<ServiceResponse<SponsorshipPlanModel>> CreateSponserShipPlan(CreateSponsorshipPlanDto createSponsorship);
    Task<ServiceResponse<List<SponsorshipPlanModel>>> GetClientSponsorShipPlans(string userName);
}
