﻿using Core;
using VenekeBankBLL.DomainModels.CommunityProjects;
using VenekeBankBLL.DomainModels.SponsorShip;

namespace VenekeBankDomain.Abstractions;

public interface ICommunityProjectService
{
    Task<ServiceResponse<List<CommunityProjectsDto>>> GetAllCommunityProjectsAsync();

    Task<ServiceResponse<CommunityProjectsDto>> CreateCommunityProject(CommunityProjectsCreateDto createNewProject);

    Task<ServiceResponse<List<CommunityProjectsDto>>> GetCommunityProjectsByUserNameAsync(string username);
}
