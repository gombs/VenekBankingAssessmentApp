﻿using Core;
using VenekeBankBLL.DomainModels.Users;

namespace VenekeBankBLL.Abstractions;

public interface IUserAuthService
{
    Task<ServiceResponse<int>> Register(UserModel user, string password);

    Task<ServiceResponse<string>> Login(string username, string password);

    Task<bool> UserExists(string username);
}
