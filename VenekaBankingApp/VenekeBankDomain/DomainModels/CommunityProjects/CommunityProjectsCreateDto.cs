﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VenekeBankBLL.DomainModels.CommunityProjects;

public class CommunityProjectsCreateDto
{
    public string Name { get; set; }

    public string Description { get; set; }

    public DateTime StartDate { get; set; }

    public DateTime FinishDate { get; set; }

    public double TotalFundsRequired { get; set; } = 0.0;

    public double TotalFundsRaised { get; set; } = 0.0;

    public string CreatedBy { get; set; }
}
