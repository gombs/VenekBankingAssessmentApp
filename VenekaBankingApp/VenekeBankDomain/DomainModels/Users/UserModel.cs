﻿namespace VenekeBankBLL.DomainModels.Users;

public class UserModel
{
    public int Id { get; set; }

    public string Username { get; set; } = string.Empty;

    public byte[] PasswordHash { get; set; }

    public byte[] PasswordSalt { get; set; }

    public List<string> Roles { get; set; }
}
