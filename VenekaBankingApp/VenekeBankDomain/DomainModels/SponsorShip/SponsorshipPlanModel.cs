﻿using Core.Enums;
using VenekeBankBLL.DomainModels.CommunityProjects;
using VenekeBankBLL.DomainModels.Users;

namespace VenekeBankBLL.DomainModels.SponsorShip;

public class SponsorshipPlanModel
{
    public int SponsorshipPlanId { get; set; }

    public UserModel User { get; set; }

    public CommunityProjectsDto CommunityProject { get; set; }

    public EnumSourceOfFundsType SourceOfFunds { get; set; }

    public EnumPaymentFrequenceType PaymentFrequency { get; set; }
}
