﻿using Core.Enums;

namespace VenekeBankBLL.DomainModels.SponsorShip;

public class CreateSponsorshipPlanDto
{
    public string UserName { get; set; }

    public int CommunityProjectId { get; set; }

    public string StartDate { get; set; }

    public string FinishDate { get; set; }

    public EnumSourceOfFundsType SourceOfFunds { get; set; }

    public EnumPaymentFrequenceType PaymentFrequency { get; set; }

}


