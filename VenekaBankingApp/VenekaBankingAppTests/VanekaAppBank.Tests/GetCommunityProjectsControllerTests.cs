﻿using Core;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Net;
using System.Net.Http.Json;
using System.Xml.Linq;
using VenekeBankBLL.DomainModels.CommunityProjects;

namespace VanekaAppBank.Tests
{
    [TestFixture]
    public class GetCommunityProjectsControllerTests
    {
        public readonly HttpClient _httpClient;

        public GetCommunityProjectsControllerTests()
        {
            var webApplicationFactory = new WebApplicationFactory<Program>();

            _httpClient = webApplicationFactory.CreateClient();
        }

        [Test]
        public async Task GetAllCommunityProjects()
        {
            // Arrange
            // Act
            var requestResult = await _httpClient.GetAsync("api/CommunityProject/getallprojects");

            requestResult.EnsureSuccessStatusCode();
            var json = await requestResult.Content.ReadAsStringAsync();
            var communityProjects = JsonConvert.DeserializeObject<ServiceResponse<List<CommunityProjectsDto>>>(json);

            // ASSERT
            Assert.That(requestResult.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(json, "Object was null");
            Assert.IsNotNull(communityProjects.Data, "Object was null");
        }

        [Test]
        public async Task CreateCommunityProjects()
        {
            // ARRANGE
            var createProject = new CommunityProjectsCreateDto
            {
                Name = "",
                Description = "",
                StartDate = DateTime.Today,
                FinishDate = DateTime.Today.AddDays(7),
                TotalFundsRequired = 0.0,
                TotalFundsRaised = 0.0,
            };

            // ACT
            var requestResult = await _httpClient.PostAsJsonAsync("api/CommunityProject/Create", createProject);

            // ASSERT
            requestResult.EnsureSuccessStatusCode();
            var json = await requestResult.Content.ReadAsStringAsync();
            var taxNameResponse = JsonConvert.DeserializeObject<ServiceResponse<CommunityProjectsDto>>(json);

            Assert.AreEqual(taxNameResponse.IsSuccessfull, true);
        }
    }
}