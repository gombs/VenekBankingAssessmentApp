﻿namespace Core;

public class ServiceResponse<T>
{
    public T? Data { get; set; } = default(T?);
    public bool IsSuccessfull { get; set; } = false;
    public string Message { get; set; } = string.Empty;
}