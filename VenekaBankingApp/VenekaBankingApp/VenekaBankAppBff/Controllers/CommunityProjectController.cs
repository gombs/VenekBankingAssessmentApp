﻿using Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VenekeBankBLL.DomainModels.CommunityProjects;
using VenekeBankDomain.Abstractions;

namespace VenekaBankAppBff.Controllers;

[ApiController]
[Route("api/[controller]")]
[Authorize]
public class CommunityProjectController : Controller
{
    private readonly ICommunityProjectService _communityProjectService;

    public CommunityProjectController(ICommunityProjectService communityProjectService)
    {
        _communityProjectService = communityProjectService;
    }

    /// <summary>
    /// Get Sponsored Community projects
    /// </summary>
    /// <returns></returns>
    [HttpGet("getallprojects")]
    public async Task<ServiceResponse<List<CommunityProjectsDto>>> GetCommunityProjects()
    {
        return await _communityProjectService.GetAllCommunityProjectsAsync();
    }

    /// <summary>
    /// Get Sponsored Community projects by username
    /// </summary>
    /// <returns></returns>
    [HttpGet("getallprojectsbyusername")]
    public async Task<ServiceResponse<List<CommunityProjectsDto>>> GetCommunityProjectsByUserName(string username)
    {
        return await _communityProjectService.GetCommunityProjectsByUserNameAsync(username);
    }

    /// <summary>
    /// Create a new Community projects
    /// </summary>
    /// <returns></returns>
    [HttpPost("create")]
    public async Task<ServiceResponse<CommunityProjectsDto>> CreateCommunityProjects(CommunityProjectsCreateDto createNewProject)
    {
        return await _communityProjectService.CreateCommunityProject(createNewProject);
    }
}
