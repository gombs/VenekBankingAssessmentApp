﻿using Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VenekeBankBLL.Abstractions;
using VenekeBankBLL.DomainModels.SponsorShip;

namespace VenekaBankAppBff.Controllers;

[ApiController]
[Route("api/[controller]")]
[Authorize]
public class VenekBankingController : Controller
{
    private readonly IVenekBankingService _venekBankingService;

    public VenekBankingController(IVenekBankingService venekBankingService) 
        => _venekBankingService = venekBankingService;

    /// <summary>
    /// Create a SponserShip plan
    /// </summary>
    /// <returns></returns>
    [HttpPost("CreateSponsorShipPlan")]
    public async Task<ServiceResponse<SponsorshipPlanModel>> CreateSponserShipPlan(CreateSponsorshipPlanDto createSponsorship) 
        => await _venekBankingService.CreateSponserShipPlan(createSponsorship);

    /// <summary>
    /// Get client list sponsor plans
    /// </summary>
    /// <returns></returns>
    [HttpGet("GetClientSponsorShipPlans")]
    public async Task<ServiceResponse<List<SponsorshipPlanModel>>> GetClientSponsorShipPlans(string userName) 
        => await _venekBankingService.GetClientSponsorShipPlans(userName);

}
