﻿using Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using VenekeBankBLL.Abstractions;
using VenekeBankBLL.DomainModels.CommunityProjects;
using VenekeBankBLL.DomainModels.Users;

namespace VenekaBankAppBff.Controllers;

[ApiController]
[Route("api/[controller]")]
public class UserController : Controller
{
    private readonly IUserAuthService _userAuthService;

    public UserController(IUserAuthService userAuthService)
    {
        _userAuthService = userAuthService;
    }


    /// <summary>
    /// Create a new Customer
    /// </summary>
    /// <returns></returns>
    [HttpPost("register")]
    public async Task<ActionResult<ServiceResponse<int>>> Register(UserRegisterDto request)
    {
        var response = await _userAuthService.Register(
            new UserModel { Username = request.Username }, request.Password
        );

        if (!response.IsSuccessfull)
        {
            return BadRequest(response);
        }

        return Ok(response);
    }

    /// <summary>
    /// Login and get user token
    /// </summary>
    /// <returns></returns>
    [HttpPost("login")]
    public async Task<ActionResult<ServiceResponse<string>>> Login(UserLoginDto request)
    {
        var response = await _userAuthService.Login(request.Username, request.Password);

        if (!response.IsSuccessfull)
        {
            return BadRequest(response);
        }

        return Ok(response);
    }

}
