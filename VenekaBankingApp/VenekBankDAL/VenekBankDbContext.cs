﻿using Microsoft.EntityFrameworkCore;
using VenekBankDAL.Entities;

namespace VenekBankDAL
{
    public class VenekBankDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase(databaseName: "VenekBankingDb");
        }

        public DbSet<CommunityProject> CommunityProjects { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<SponsorshipPlan> SponsorshipPlans { get; set; }
    }
}