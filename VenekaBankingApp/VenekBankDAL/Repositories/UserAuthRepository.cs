﻿using Core;
using Microsoft.EntityFrameworkCore;
using VenekBankDAL.Abstractions;
using VenekBankDAL.Entities;

namespace VenekBankDAL.Repositories
{
    public class UserAuthRepository : IUserAuthRepository
    {
        private readonly VenekBankDbContext _context;

        public UserAuthRepository(VenekBankDbContext context)
        {
            _context = context;
        }

        public async Task<User> GetUserByUsernameAsync(string userName)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Username.Equals(userName, StringComparison.OrdinalIgnoreCase));
            
            return user;
        }

        public async Task<ServiceResponse<int>> Register(User user, string password)
        {
            ServiceResponse<int> response = new ServiceResponse<int>();            

            if (await UserExists(user.Username))
            {
                response.IsSuccessfull = false;
                response.Message = "User already exists.";
                return response;
            }

            CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);

            user.CreatedBy = user.Username;
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            _context.Users.Add(user);
            
            var rowsAffected = await _context.SaveChangesAsync();

            if (rowsAffected > 0)
            {
                response.IsSuccessfull = true;
                response.Data = user.Id;
                response.Message = "Successfully created a new User.";
            }

            return response;
        }

        public async Task<bool> UserExists(string username)
        {
            if (await _context.Users.AnyAsync(u => u.Username.ToLower() == username.ToLower()))
            {
                return true;
            }
            return false;
        }

        #region Helper Functions
        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
        #endregion
    }
}
