﻿using Microsoft.EntityFrameworkCore;
using VenekBankDAL.Abstractions;
using VenekBankDAL.Entities;

namespace VenekBankDAL.Repositories;

public class VenekeBankRepository : IVenekeBankRepository
{
    private readonly VenekBankDbContext _venekBankDbContext;

    public VenekeBankRepository(VenekBankDbContext venekBankDbContext)
    {
        _venekBankDbContext = venekBankDbContext;
    }

    public async Task<CommunityProject> CreateCommunityProjectAsync(CommunityProject communityProject)
    {
        var entityModel = _venekBankDbContext.CommunityProjects.Add(communityProject);

        await _venekBankDbContext.SaveChangesAsync();

        var createdEntityModel = entityModel.Entity;

        return createdEntityModel;
    }

    public async Task<SponsorshipPlan> CreateSponsorshipPlanAsync(SponsorshipPlan createSponsorship)
    {
        var entityModel = _venekBankDbContext.SponsorshipPlans.Add(createSponsorship);

        await _venekBankDbContext.SaveChangesAsync();

        var createdEntityModel = entityModel.Entity;

        return createdEntityModel;
    }

    public async Task<List<CommunityProject>> GetAllCommunityProjectsAsync()
    {
        var result = await _venekBankDbContext.CommunityProjects.ToListAsync();

        if (result is not null)
            return result;

        return null;
    }

    public async Task<List<CommunityProject>> GetClientCommunityProjectsAsync(string username)
    {
        var result = await _venekBankDbContext.CommunityProjects
                    .Where(c => c.CreatedBy.Equals(username, StringComparison.InvariantCultureIgnoreCase))
                    .ToListAsync();

        if (result is not null)
            return result;

        return null;
    }

    public async Task<List<SponsorshipPlan>> GetClientSponsorShipPlansAsync(string userName)
    {
        var result = await _venekBankDbContext.SponsorshipPlans
            .Include(c => c.User)
            .Where(sponsorShipPlan => sponsorShipPlan.User.Username == userName).ToListAsync();

        if (result is not null)
            return result;

        return null;
    }

    public async Task<CommunityProject> GetCommunityProjectByIdAsync(int projectId)
    {
        var result = await _venekBankDbContext.CommunityProjects
                               .FirstOrDefaultAsync( y => y.Id == projectId);

        if (result is not null)
            return result;

        return null;
    }
}
