﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VenekBankDAL.Abstractions;
using VenekBankDAL.Repositories;

namespace VenekBankDAL;

public static class Extensions
{
    public static IServiceCollection AddVenekeBankDALServiceCollections(this IServiceCollection servicesCollection, IConfiguration configuration)
    {
        servicesCollection.AddDbContext<VenekBankDbContext>();

        return servicesCollection
            .AddTransient<IVenekeBankRepository, VenekeBankRepository>()
            .AddTransient<IUserAuthRepository, UserAuthRepository>();
    }
}
