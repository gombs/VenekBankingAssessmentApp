﻿using VenekBankDAL.Entities;

namespace VenekBankDAL.Abstractions;

public interface IVenekeBankRepository
{
    Task<CommunityProject> CreateCommunityProjectAsync(CommunityProject communityProject);

    Task<SponsorshipPlan> CreateSponsorshipPlanAsync(SponsorshipPlan createSponsorship);

    Task<List<CommunityProject>> GetAllCommunityProjectsAsync();

    Task<List<CommunityProject>> GetClientCommunityProjectsAsync(string username);

    Task<List<SponsorshipPlan>> GetClientSponsorShipPlansAsync(string userName);

    Task<CommunityProject> GetCommunityProjectByIdAsync(int porjectId);
}
