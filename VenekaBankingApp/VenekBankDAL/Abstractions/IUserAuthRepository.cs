﻿using Core;
using VenekBankDAL.Entities;

namespace VenekBankDAL.Abstractions;

public interface IUserAuthRepository
{
    Task<User> GetUserByUsernameAsync(string userName);

    Task<ServiceResponse<int>> Register(User user, string password);

    Task<bool> UserExists(string username);
}
