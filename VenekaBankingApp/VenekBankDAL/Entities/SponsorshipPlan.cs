﻿using Core.Enums;

namespace VenekBankDAL.Entities;

public class SponsorshipPlan : BaseEntity
{
    public int SponsorshipPlanId { get; set; }

    public User User { get; set; }

    public CommunityProject CommunityProject { get; set; }

    public string StartDate { get; set; }

    public string FinishDate { get; set; }

    public EnumSourceOfFundsType SourceOfFunds { get; set; }

    public EnumPaymentFrequenceType PaymentFrequency { get; set; }
}
