﻿using System.ComponentModel.DataAnnotations;

namespace VenekBankDAL.Entities;

public class BaseEntity
{
    [Required]
    public string CreatedBy { get; set; }

    [Required]
    public DateTime DateCreated { get; set; } = DateTime.Now;

    public string UpdatedBy { get; set; }

    public DateTime DateUpdated { get; set; } = DateTime.Now;
}
