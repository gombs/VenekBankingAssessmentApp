﻿namespace VenekBankDAL.Entities;

public class CommunityProject : BaseEntity
{
    public int Id { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public string StartDate { get; set; }

    public string FinishDate { get; set; }

    public double TotalFundsRequired { get; set; }

    public double TotalFundsRaised { get; set; }
}
